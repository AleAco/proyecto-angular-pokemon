import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  private url = "https://pokeapi.co/api/v2/pokemon";

  getPokemon(offset : number, limit : number) {
    const url = `${this.url}?offset=${offset}&limit=${limit}`;
    return this.http.get(url);
  }

  getPokemonByName(name: string): Observable<any> {
      const url = `${this.url}/`+name;
      return this.http.get<any>(url);
  }

  getPokemonByUrl(url: string): Observable<any> {
    return this.http.get<any>(url);
  }
}
