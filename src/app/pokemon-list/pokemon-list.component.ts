import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  public pokemones: any;
  public pokemon: any;
  private paginaActual: number;
  private pokemonesPorPagina: number;

  constructor(private pokeapi: PokemonService)
  {
    this.paginaActual = 0;
    this.pokemonesPorPagina = 10;
  }

  ngOnInit() {
    this.getPokemons();
    const backButton = document.getElementById('back');
    const details = document.getElementById('details');
    const searchButton = document.getElementById('search');

    if (searchButton && details)
    {
        searchButton.addEventListener('click', () => {
          details.classList.add("right-panel-active");
      });
    }
    if (backButton && details)
    {
      backButton.addEventListener('click', () => {
        details.classList.remove("right-panel-active");
      });
    }
  }

  getPokemons(): void
  {
    const limit = this.pokemonesPorPagina;
    const offset = this.paginaActual * limit;
    this.pokeapi.getPokemon(offset, limit).subscribe((res: any) => {
      this.pokemones = res.results;
    });
  }

  async getPokemonDetails(url:any)
  {
    this.pokeapi.getPokemonByUrl(url).subscribe((data:any) =>
    {
      const details = document.getElementById('details');
      if (details)
      {
        details.classList.add("right-panel-active");
      }
      this.pokemon = data;
    });
  }

  getPokemonName(name:string):void {
    this.pokemon = null;
    if (name) {
      this.pokeapi.getPokemonByName(name.toLocaleLowerCase()).subscribe(
        (data:any) => {
        this.pokemon = data;
      },
      (error) => {console.log('Error obteniendo pokemon: ', error)})
    }
  }

  getPreviousList():void
  {
    if (this.paginaActual > 0)
    {
      --this.paginaActual;
      this.getPokemons();
    }
  }

  getNextList():void
  {
    ++this.paginaActual;
    this.getPokemons();
  }
}
