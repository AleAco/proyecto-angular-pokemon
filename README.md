# Proyecto Pokemon

Se creó una aplicación web con HTML, CSS, TypeScript y Angular, la cual utiliza la **API REST PokéAPI** (pokeapi.co) para obtener la información del pokémon deseado. Para eso, se puede introducir en el buscador el nombre o el id de algún pokemon y luego se mostrará algunas de sus características.
Asimismo, se pueden observar todos los pokemones disponibles.

Para ejecutar el ejemplo, basta correr **ng serve** o visualizarlo en https://getpokemonale.netlify.app


